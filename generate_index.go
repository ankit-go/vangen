package main

import (
	"fmt"
	"html/template"
	"io"
)

func generate_index(w io.Writer, domain string, r []repository) error {
	const html = `<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
<style>
		body { max-width: 40em; margin: auto; font-family: -apple-system,
		BlinkMacSystemFont,"Segoe UI", "Roboto", "Oxygen", "Overpass", "Ubuntu",
		"Cantarell", "Fira Sans","Droid Sans", "Helvetica Neue", sans-serif;
		padding: 0 2%; }
</style>
</head>
<body>
<header>
<h1>Go Modules</h1>
<hr />
</header>
{{ if .MainRepositories }}
<h3>Tools:</h3>
<ul>
{{range $_, $r := .MainRepositories -}}
<li>
<a href="/{{$r.Prefix}}">{{$r.Prefix}}</a>
{{if .Subs -}}<ul>{{end -}}
{{range $_, $s := .Subs -}}{{if not $s.Hidden -}}<li><a href="/{{$r.Prefix}}/{{$s.Name}}">{{$s.Name}}</a></li>{{end -}}{{end -}}
{{if .Subs -}}</ul>{{end -}}
</li>
{{end -}}
</ul>

{{ end }}
{{ if .PackageRepositories }}
<h3>Libraries:</h3>

<ul>
{{range $_, $r := .PackageRepositories -}}
<li>
<a href="/{{$r.Prefix}}">{{$r.Prefix}}</a>
{{if .Subs -}}<ul>{{end -}}
{{range $_, $s := .Subs -}}{{if not $s.Hidden -}}<li><a href="/{{$r.Prefix}}/{{$s.Name}}">{{$s.Name}}</a></li>{{end -}}{{end -}}
{{if .Subs -}}</ul>{{end -}}
</li>
{{end -}}
</ul>
{{ end }}
<footer style="font-size: 0.8em;margin-top:5vh;text-align:center;">
<hr />
Generated using a modified version of <a
href="https://4d63.com/vangen">vangen</a>.
</footer>
</body>
</html>`

	tmpl, err := template.New("").Parse(html)
	if err != nil {
		return fmt.Errorf("error loading template: %v", err)
	}

	mainRepositories := []repository{}
	packageRepositories := []repository{}
	for _, r := range r {
		if r.Main {
			mainRepositories = append(mainRepositories, r)
		} else {
			packageRepositories = append(packageRepositories, r)
		}
	}

	data := struct {
		Domain              string
		MainRepositories    []repository
		PackageRepositories []repository
	}{
		Domain:              domain,
		MainRepositories:    mainRepositories,
		PackageRepositories: packageRepositories,
	}

	err = tmpl.ExecuteTemplate(w, "", data)
	if err != nil {
		return fmt.Errorf("generating template: %v", err)
	}

	return nil
}
